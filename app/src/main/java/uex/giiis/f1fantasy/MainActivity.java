package uex.giiis.f1fantasy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import uex.giiis.f1fantasy.recyclerViews.EscuderiasExpandableListAdapter;
import uex.giiis.f1fantasy.recyclerViews.LigasRecyclerViewAdapter;
import uex.giiis.f1fantasy.ui.DetallesGPActivity;
import uex.giiis.f1fantasy.ui.DetallesLigaActivity;
import uex.giiis.f1fantasy.ui.DetallesPilotoActivity;
import uex.giiis.f1fantasy.ui.LoginActivity;
import uex.giiis.f1fantasy.ui.PerfilActivity;
import uex.giiis.f1fantasy.ui.popUps.CrearLigaPopup;
import uex.giiis.f1fantasy.ui.popUps.UnirseLigaPopup;


public class MainActivity extends AppCompatActivity implements LigasRecyclerViewAdapter.OnListInteractionListener, EscuderiasExpandableListAdapter.OnListPilotosListener {

    LigasRecyclerViewAdapter adapter;

    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_ligas, R.id.navigation_calendario, R.id.navigation_clasificaciones, R.id.navigation_escuderias)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        //NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_app_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void crearLiga (View view) {
        Intent intent = new Intent(this, CrearLigaPopup.class);
        startActivity(intent);
    }

    public void editarPerfil (MenuItem item) {
        Intent intent = new Intent(this, PerfilActivity.class);
        startActivity(intent);
    }

    public void cerrarSesion (MenuItem item) {
        sharedPref = MainActivity.this.getSharedPreferences("usuario", MainActivity.this.MODE_PRIVATE);

        int id_usuario = sharedPref.getInt("id_usuario", -1);

        if (id_usuario != -1) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("id_usuario", -1);
            editor.commit();

            Intent intent = new Intent (this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onListLigas(int id) {
        Intent intent = new Intent (this, DetallesLigaActivity.class);
        intent.putExtra("ID_LIGA", Integer.toString(id));
        startActivity(intent);
    }

    @Override
    public void onListCalendario(String id) {
        Intent intent = new Intent (this, DetallesGPActivity.class);
        intent.putExtra("ID_GP", id);
        startActivity(intent);
    }

    public void unirseLiga (View view) {
        Intent intent = new Intent(this, UnirseLigaPopup.class);
        startActivity(intent);
    }


    @Override
    public void onListPilotos(String id) {
        Intent intent = new Intent (this, DetallesPilotoActivity.class);
        intent.putExtra("ID_PILOTO", id);
        startActivity(intent);
    }
}