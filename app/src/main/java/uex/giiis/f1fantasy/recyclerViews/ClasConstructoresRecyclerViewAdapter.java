package uex.giiis.f1fantasy.recyclerViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.generatedPojos.ConstructorStanding;


public class ClasConstructoresRecyclerViewAdapter extends RecyclerView.Adapter<ClasConstructoresRecyclerViewAdapter.ViewHolder> {

    private List<ConstructorStanding> mValues;
    private Context mContext;

    public ClasConstructoresRecyclerViewAdapter(List<ConstructorStanding> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.clasificacion_constructores_fila, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        ConstructorStanding item = mValues.get(position);

        holder.mItem = item;
        holder.mPosicionView.setText(item.getPosition());
        holder.mConstructorView.setText(item.getConstructor().getName());
        holder.mPuntosView.setText(item.getPoints());

    }


    @Override
    public int getItemCount() {
        int resultado = 0;
        if(mValues != null){
            resultado = mValues.size();
        }
        return resultado;
    }


    public List<ConstructorStanding> getmValues() {
        return mValues;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mPosicionView;
        public final TextView mConstructorView;
        public final TextView mPuntosView;
        public ConstructorStanding mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPosicionView = (TextView)  view.findViewById(R.id.posicion_clas_constructores);
            mConstructorView = (TextView)  view.findViewById(R.id.piloto_constructor_clas_constructores);
            mPuntosView = (TextView)  view.findViewById(R.id.puntos_clas_constructores);


        }

    }

    public void swap(List<ConstructorStanding> items){
        mValues = items;
        notifyDataSetChanged();
    }

}

