package uex.giiis.f1fantasy.recyclerViews;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;

import static uex.giiis.f1fantasy.ColeccionFotos.obtenerLogoConstructor;


public class UsuariosExpandableListAdapter extends BaseExpandableListAdapter {

    private List<String> userList;
    private Map<String, List<String>> mapUsersDrivers;
    private Context context;

    public UsuariosExpandableListAdapter(List<String> userList, Map<String, List<String>> mapUsersDrivers, Context context) {
        this.userList = userList;
        this.mapUsersDrivers = mapUsersDrivers;
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return userList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mapUsersDrivers.get(userList.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return userList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mapUsersDrivers.get(userList.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String s = (String) getGroup(groupPosition);
        convertView = LayoutInflater.from(context).inflate(R.layout.user_driver_group, null);
        TextView user_driver_group = (TextView) convertView.findViewById(R.id.user_driver_group);
        user_driver_group.setText(s);

        ImageView imgExpandCollapse = (ImageView) convertView.findViewById(R.id.arrow);
        if(isExpanded){
            imgExpandCollapse.setImageResource(R.drawable.ic_arrow_up_white);
        }
        else{
            imgExpandCollapse.setImageResource(R.drawable.ic_arrow_down_white);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        String s = (String) getChild(groupPosition, childPosition);
        convertView = LayoutInflater.from(context).inflate(R.layout.user_driver_child, null);
        TextView user_driver_child = (TextView) convertView.findViewById(R.id.user_driver_child);
        user_driver_child.setText(s);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public void swap(List<String> userList, Map<String, List<String>> mapUsersDrivers){
        this.userList = userList;
        this.mapUsersDrivers = mapUsersDrivers;
        notifyDataSetChanged();
    }

}

