package uex.giiis.f1fantasy.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.ConstructorStanding;
import uex.giiis.f1fantasy.pojo.ConstructorStandingComplete;

@Dao
public interface ConstructorStandingDao {

    @Query("SELECT * FROM ConstructorStanding")
    public List<ConstructorStanding> getAll();

    @Query("SELECT * FROM ConstructorStanding WHERE idConstructor = :idConstructor")
    public List<ConstructorStanding> getFromConstructor(String idConstructor);

    @Query("SELECT * FROM ConstructorStanding WHERE round = :idStanding")
    public List<ConstructorStanding> getFromStanding(int idStanding);

    @Transaction
    @Query("SELECT * FROM ConstructorStanding WHERE round = :idStanding")
    public LiveData<List<ConstructorStandingComplete>> getFromStandingComplete(int idStanding);

    @Query("SELECT * FROM ConstructorStanding WHERE idConstructor = :idConstructor AND round = :idStanding")
    public ConstructorStanding get(String idConstructor, int idStanding);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert (ConstructorStanding constructorPosition);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll (List<ConstructorStanding> constructorPosition);

    @Update
    public int update (ConstructorStanding constructorPosition);

    @Delete
    public int delete (ConstructorStanding constructorPosition);
}
