package uex.giiis.f1fantasy.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Race;

@Dao
public interface RaceDao {

    @Query("SELECT * FROM Race")
    public LiveData<List<Race>> getAll();

    @Query("SELECT * FROM race WHERE round = :gpId")
    public LiveData<Race> get(int gpId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll (List<Race> races);

    @Update
    public int updateAll (List<Race> races);

    @Query("DELETE FROM Race")
    void deleteAll();
}
