package uex.giiis.f1fantasy.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import uex.giiis.f1fantasy.pojo.League;

@Dao
public interface LeagueDao {

    @Query("SELECT * FROM league")
    public List<League> getAll();

    @Query("SELECT * FROM league WHERE name = :leagueName")
    public League get(String leagueName);

    @Query("SELECT * FROM league WHERE name = :leagueName AND password = :pass")
    public League get(String leagueName, String pass);

    @Query("SELECT * FROM league WHERE id = :leagueId")
    public LiveData<League> get(int leagueId);

    @Query("SELECT * FROM league WHERE id = :leagueId")
    public League getLeague(int leagueId);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    public void insert (League league);

    @Update
    public int update (League league);

    @Delete
    public int delete (League league);

    @Query("DELETE FROM league WHERE id = :leagueId")
    public void delete (int leagueId);
}
