package uex.giiis.f1fantasy.roomdb;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.pojo.DriverConstructors;
import uex.giiis.f1fantasy.generatedPojos.ConstructorStanding;
import uex.giiis.f1fantasy.generatedPojos.DriverStanding;
import uex.giiis.f1fantasy.generatedPojos.Result;
import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.pojo.Information;
import uex.giiis.f1fantasy.pojo.League;
import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.pojo.UserLeagues;

@Database(entities = {Constructor.class, ConstructorStanding.class, DriverStanding.class,
        Driver.class, DriverConstructors.class, Result.class, Race.class,
        League.class, User.class, UserLeagues.class, Information.class}, version = 1)
public abstract class F1Database extends RoomDatabase {
    private static F1Database instance;

    public static F1Database getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, F1Database.class, "f1.db").build();
        }
        return instance;
    }

    public abstract ConstructorDao constructorDao();
    public abstract ConstructorStandingDao constructorStandingDao();
    public abstract DriverConstructorsDao driverConstructorsDao();
    public abstract DriverDao driverDao();
    public abstract DriverStandingDao driverStandingDao();
    public abstract ResultDao resultDao();
    public abstract RaceDao raceDao();
    public abstract LeagueDao leagueDao();
    public abstract UserDao userDao();
    public abstract UserLeaguesDao userLeaguesDao();
    public abstract InformationDao informationDao();

}
