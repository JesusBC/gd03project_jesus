package uex.giiis.f1fantasy.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.pojo.DriverComplete;

@Dao
public interface DriverDao {

    @Query("SELECT * FROM driver")
    public LiveData<List<Driver>> getAll();

    @Query("SELECT * FROM driver WHERE driverId = :driverId")
    public Driver get(String driverId);

    @Transaction
    @Query("SELECT * FROM driver WHERE driverId = :driverId")
    public LiveData<DriverComplete> getCompleteDrivers(String driverId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll (List<Driver> drivers);

    @Update
    public int updateAll (List<Driver> drivers);

    @Query("DELETE FROM driver")
    void deleteAll();

}
