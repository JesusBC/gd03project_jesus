package uex.giiis.f1fantasy.roomdb;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.pojo.DriverConstructors;

@Dao
public interface DriverConstructorsDao {

    @Query("SELECT * FROM DriverConstructors")
    public List<DriverConstructors> getAll();

    @Query("SELECT * FROM DriverConstructors WHERE idDriver = :idDriver")
    public List<DriverConstructors> getFromDriver(String idDriver);

    @Query("SELECT * FROM DriverConstructors WHERE idConstructor = :idConstructor")
    public List<DriverConstructors> getFromConstructor(String idConstructor);

    @Query("SELECT * FROM DriverConstructors WHERE idDriver = :idDriver AND idConstructor = :idConstructor")
    public DriverConstructors get(String idDriver, String idConstructor);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert (DriverConstructors driverConstructors);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll (List<DriverConstructors> driverConstructors);

    @Update
    public int update (DriverConstructors driverConstructors);

    @Delete
    public int delete (DriverConstructors driverConstructors);
}
