package uex.giiis.f1fantasy;

import android.content.Context;

import uex.giiis.f1fantasy.factories.CalendarioViewModelFactory;
import uex.giiis.f1fantasy.factories.ClasificacionesConstructoresViewModelFactory;
import uex.giiis.f1fantasy.factories.ClasificacionesPilotosViewModelFactory;
import uex.giiis.f1fantasy.factories.CrearLigaViewModelFactory;
import uex.giiis.f1fantasy.factories.DetallesGPViewModelFactory;
import uex.giiis.f1fantasy.factories.DetallesLigaViewModelFactory;
import uex.giiis.f1fantasy.factories.DetallesPilotoViewModelFactory;
import uex.giiis.f1fantasy.factories.EscuderiasViewModelFactory;
import uex.giiis.f1fantasy.factories.LigasViewModelFactory;
import uex.giiis.f1fantasy.factories.LoginViewModelFactory;
import uex.giiis.f1fantasy.factories.PerfilViewModelFactory;
import uex.giiis.f1fantasy.factories.RegisterViewModelFactory;
import uex.giiis.f1fantasy.factories.SeleccionarPilotosViewModelFactory;
import uex.giiis.f1fantasy.factories.SplashViewModelFactory;
import uex.giiis.f1fantasy.factories.UnirseLigaViewModelFactory;
import uex.giiis.f1fantasy.factories.VerPilotosElegidosViewModelFactory;
import uex.giiis.f1fantasy.network.F1NetworkDataSource;
import uex.giiis.f1fantasy.repository.DaoCollection;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.roomdb.F1Database;

public class AppContainer {

    private F1Database database;
    private F1NetworkDataSource networkDataSource;
    public F1Repository repository;
    public DaoCollection daoCollection;

    public CalendarioViewModelFactory calendarioFactory;
    public ClasificacionesPilotosViewModelFactory clasificacionesPilotosFactory;
    public ClasificacionesConstructoresViewModelFactory clasificacionesConstructoresFactory;
    public EscuderiasViewModelFactory escuderiasFactory;
    public LigasViewModelFactory ligasFactory;
    public CrearLigaViewModelFactory crearLigaFactory;
    public UnirseLigaViewModelFactory unirseLigaFactory;
    public DetallesGPViewModelFactory detallesGPFactory;
    public DetallesLigaViewModelFactory detallesLigaFactory;
    public DetallesPilotoViewModelFactory detallesPilotoFactory;
    public LoginViewModelFactory loginFactory;
    public PerfilViewModelFactory perfilFactory;
    public RegisterViewModelFactory registerFactory;
    public SeleccionarPilotosViewModelFactory seleccionarPilotosFactory;
    public SplashViewModelFactory splashFactory;
    public VerPilotosElegidosViewModelFactory verPilotosElegidosFactory;

    public AppContainer(Context context){
        database = F1Database.getInstance(context);
        networkDataSource = F1NetworkDataSource.getInstance(context);
        daoCollection = new DaoCollection(database.constructorDao(), database.constructorStandingDao(),
                database.driverConstructorsDao(), database.driverDao(), database.driverStandingDao(),
                database.informationDao(), database.leagueDao(), database.raceDao(), database.resultDao(),
                database.userDao(), database.userLeaguesDao(), context);
        repository = F1Repository.getInstance(daoCollection, networkDataSource);

        calendarioFactory = new CalendarioViewModelFactory(repository);
        clasificacionesPilotosFactory = new ClasificacionesPilotosViewModelFactory(repository);
        clasificacionesConstructoresFactory = new ClasificacionesConstructoresViewModelFactory(repository);
        escuderiasFactory = new EscuderiasViewModelFactory(repository);
        ligasFactory = new LigasViewModelFactory(repository);
        crearLigaFactory = new CrearLigaViewModelFactory(repository);
        unirseLigaFactory = new UnirseLigaViewModelFactory(repository);
        detallesGPFactory = new DetallesGPViewModelFactory(repository);
        detallesLigaFactory = new DetallesLigaViewModelFactory(repository);
        detallesPilotoFactory = new DetallesPilotoViewModelFactory(repository);
        loginFactory = new LoginViewModelFactory(repository);
        perfilFactory = new PerfilViewModelFactory(repository);
        registerFactory = new RegisterViewModelFactory(repository);
        seleccionarPilotosFactory = new SeleccionarPilotosViewModelFactory(repository);
        splashFactory = new SplashViewModelFactory(repository);
        verPilotosElegidosFactory = new VerPilotosElegidosViewModelFactory(repository);
    }
}
