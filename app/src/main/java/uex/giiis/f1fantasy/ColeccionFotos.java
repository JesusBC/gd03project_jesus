package uex.giiis.f1fantasy;

import android.util.Log;

public class ColeccionFotos {

    public static String [] constructorId = {"alfa", "alphatauri", "ferrari", "haas", "mclaren",
            "mercedes", "racing_point", "red_bull", "renault", "williams"};
    public static int [] resource = {R.mipmap.team_alfa, R.mipmap.team_alphatauri, R.mipmap.team_ferrari,
            R.mipmap.team_haas, R.mipmap.team_mclaren, R.mipmap.team_mercedes, R.mipmap.team_racing_point,
            R.mipmap.team_red_bull, R.mipmap.team_renault, R.mipmap.team_williams};

    public static int obtenerLogoConstructor (String constructor) {
        boolean salir = false;
        int returnResource = -1;
        for (int i = 0; i < constructorId.length && !salir; i++) {
            if (constructorId[i].equals(constructor)) {
                returnResource = resource[i];
                salir = true;
            }
        }
        if(!salir){
            returnResource = R.mipmap.team_unknown;
        }
        return returnResource;
    }


    public static String [] pilotoId = {"albon","bottas", "gasly", "giovinazzi", "grosjean", "hamilton",
            "hulkenberg", "kvyat", "latifi", "leclerc", "kevin_magnussen", "norris", "ocon", "perez"
            , "raikkonen", "ricciardo", "russell", "sainz", "stroll", "max_verstappen", "vettel"};
    public static int [] pilotoResource = {R.mipmap.driver_albon, R.mipmap.driver_bottas, R.mipmap.driver_gasly, R.mipmap.driver_giovinazzi,
            R.mipmap.driver_grosjean, R.mipmap.driver_hamilton, R.mipmap.driver_hulkenberg, R.mipmap.driver_kvyat,
            R.mipmap.driver_latifi, R.mipmap.driver_leclerc, R.mipmap.driver_magnussen, R.mipmap.driver_norris
            , R.mipmap.driver_ocon, R.mipmap.driver_perez, R.mipmap.driver_raikkonen, R.mipmap.driver_ricciardo,
            R.mipmap.driver_russell, R.mipmap.driver_sainz, R.mipmap.driver_stroll, R.mipmap.driver_verstappen,
            R.mipmap.driver_vettel};

    public static int obtenerImagenPiloto (String piloto) {
        boolean salir = false;
        int returnResource = -1;
        for (int i = 0; i < pilotoId.length && !salir; i++) {
            if (pilotoId[i].equals(piloto)) {
                returnResource = pilotoResource[i];
                salir = true;
            }
        }
        if(!salir){
            returnResource = R.mipmap.team_unknown;
        }
        return returnResource;
    }


    public static String [] pilotoBackgroundId = {"albon","bottas", "gasly", "giovinazzi", "grosjean", "hamilton",
            "hulkenberg", "kvyat", "latifi", "leclerc", "kevin_magnussen", "norris", "ocon", "perez"
            , "raikkonen", "ricciardo", "russell", "sainz", "stroll", "max_verstappen", "vettel"};
    public static int [] pilotoBackgroundResource = {R.mipmap.background_albon, R.mipmap.background_bottas, R.mipmap.background_gasly, R.mipmap.background_giovinazzi,
            R.mipmap.background_grosjean, R.mipmap.background_hamilton, R.mipmap.background_hulkenberg, R.mipmap.background_kvyat,
            R.mipmap.background_latifi, R.mipmap.background_leclerc, R.mipmap.background_magnussen, R.mipmap.background_norris
            , R.mipmap.background_ocon, R.mipmap.background_perez, R.mipmap.background_raikkonen, R.mipmap.background_ricciardo,
            R.mipmap.background_russell, R.mipmap.background_sainz, R.mipmap.background_stroll, R.mipmap.background_verstappen,
            R.mipmap.background_vettel};

    public static int obtenerFondoPiloto (String piloto) {
        boolean salir = false;
        int returnResource = -1;
        for (int i = 0; i < pilotoBackgroundId.length && !salir; i++) {
            if (pilotoBackgroundId[i].equals(piloto)) {
                returnResource = pilotoBackgroundResource[i];
                salir = true;
            }
        }
        if(!salir){
            returnResource = R.mipmap.background_unknown;
        }
        return returnResource;
    }


    public static String [] nacionalidadId = {"Argentinian", "Australian", "Austrian", "Azerbaijani", "Barhraini",
            "Belgian", "Brazilian", "Canadian", "Chinese", "Colombian", "European", "Finnish", "French",
            "German", "Hungarian", "Indonesian", "Italian", "Japanese", "Mexican", "Monegasque", "Dutch",
            "Portuguese", "Russian", "San Marinese", "Singaporean", "Spanish", "Swedish", "Swiss",
            "Turkish", "British", "American", "Thai", "Danish"};
    public static int [] nacionalidadResource = {R.mipmap.argentina, R.mipmap.australia, R.mipmap.austria,
            R.mipmap.azerbaijan, R.mipmap.bahrain, R.mipmap.belgium, R.mipmap.brazil,
            R.mipmap.canada, R.mipmap.china, R.mipmap.colombia, R.mipmap.europe,
            R.mipmap.finland, R.mipmap.france, R.mipmap.germany, R.mipmap.hungary,
            R.mipmap.indonesia, R.mipmap.italy, R.mipmap.japan, R.mipmap.mexico,
            R.mipmap.monaco, R.mipmap.netherlands, R.mipmap.portugal, R.mipmap.russia,
            R.mipmap.san_marino, R.mipmap.singapur, R.mipmap.spain, R.mipmap.sweden,
            R.mipmap.switzerland, R.mipmap.turkey, R.mipmap.uk,
            R.mipmap.usa, R.mipmap.thailand, R.mipmap.denmark};


    public static int obtenerNacionalidad (String pais) {
        boolean salir = false;
        int returnResource = -1;
        for (int i = 0; i < nacionalidadId.length && !salir; i++) {
            if (nacionalidadId[i].equals(pais)) {
                returnResource = nacionalidadResource[i];
                salir = true;
            }
        }
        if (returnResource == -1) {
            returnResource = R.mipmap.unknown;
        }
        return returnResource;
    }
}
