package uex.giiis.f1fantasy.pojo;

public class IdPilotoIdLiga {

    private int idPiloto;
    private int idLiga;

    public IdPilotoIdLiga(int idPiloto, int idLiga) {
        this.idPiloto = idPiloto;
        this.idLiga = idLiga;
    }

    public int getIdPiloto() {
        return idPiloto;
    }

    public void setIdPiloto(int idPiloto) {
        this.idPiloto = idPiloto;
    }

    public int getIdLiga() {
        return idLiga;
    }

    public void setIdLiga(int idLiga) {
        this.idLiga = idLiga;
    }
}
