package uex.giiis.f1fantasy.pojo;

import androidx.room.Entity;
import androidx.room.ForeignKey;

import uex.giiis.f1fantasy.generatedPojos.Driver;

import static androidx.room.ForeignKey.CASCADE;

@Entity(primaryKeys = {"user", "league"},
        foreignKeys = {@ForeignKey(entity = Driver.class,
                parentColumns = "driverId",
                childColumns = "driver1"),
                @ForeignKey(entity = Driver.class,
                        parentColumns = "driverId",
                        childColumns = "driver2"),
                @ForeignKey(entity = Driver.class,
                        parentColumns = "driverId",
                        childColumns = "driver3"),
                @ForeignKey(entity = User.class,
                parentColumns = "id",
                childColumns = "user",
                onDelete = CASCADE),
                @ForeignKey(entity = League.class,
                        parentColumns = "id",
                        childColumns = "league",
                        onDelete = CASCADE)})
public class UserLeagues {

    private int user;

    private int league;

    private String driver1;

    private String driver2;

    private String driver3;

    private int points;

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getLeague() {
        return league;
    }

    public void setLeague(int league) {
        this.league = league;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getDriver1() {
        return driver1;
    }

    public void setDriver1(String driver1) {
        this.driver1 = driver1;
    }

    public String getDriver2() {
        return driver2;
    }

    public void setDriver2(String driver2) {
        this.driver2 = driver2;
    }

    public String getDriver3() {
        return driver3;
    }

    public void setDriver3(String driver3) {
        this.driver3 = driver3;
    }
}
