package uex.giiis.f1fantasy.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.pojo.League;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.ui.DetallesLigaActivity;

/**
 * {@link ViewModel} for {@link DetallesLigaActivity}
 */
public class DetallesLigaViewModel extends ViewModel {

    private final F1Repository mRepository;
    private LiveData<List<UserLeagues>> mUserLeagues;
    private int mIdLiga;

    public DetallesLigaViewModel(F1Repository repository) {
        mRepository = repository;
        mUserLeagues = mRepository.getFromLeaguePoints();
    }

    public void setIdLiga(int idLiga){
        mIdLiga = idLiga;
        mRepository.setIdLiga(idLiga);
    }

    public void deleteLeague(int idLiga) {
        mRepository.deleteLeague(idLiga);
    }

    public void deleteUserLeague(int idLiga, int idUserLeague) {
        mRepository.deleteUserLeague(idLiga, idUserLeague);
    }

    public League getLeague(int id) {
        return mRepository.getLeague(id);
    }

    public LiveData<List<UserLeagues>> getFromLeaguePoints() { return mUserLeagues;}

}