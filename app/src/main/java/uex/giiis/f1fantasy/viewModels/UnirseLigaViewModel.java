package uex.giiis.f1fantasy.viewModels;

import androidx.lifecycle.ViewModel;

import uex.giiis.f1fantasy.pojo.League;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.ui.popUps.UnirseLigaPopup;
import uex.giiis.f1fantasy.repository.F1Repository;

/**
 * {@link ViewModel} for {@link UnirseLigaPopup}
 */
public class UnirseLigaViewModel extends ViewModel {

    private final F1Repository mRepository;

    public UnirseLigaViewModel(F1Repository repository) {
        mRepository = repository;
    }

    public League getLeague(String id, String pass) {
        return mRepository.getLeague(id, pass);
    }

    public void insertar(UserLeagues userLeagues) {
        mRepository.insertUserLeagues(userLeagues);
    }

}