package uex.giiis.f1fantasy.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import uex.giiis.f1fantasy.pojo.League;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.ui.ligas.LigasFragment;
import uex.giiis.f1fantasy.repository.F1Repository;

/**
 * {@link ViewModel} for {@link LigasFragment}
 */
public class LigasViewModel extends ViewModel {

    private final F1Repository mRepository;
    private final LiveData<List<UserLeagues>> mUserLeaguesList;


    public LigasViewModel(F1Repository repository) {
        mRepository = repository;
        mUserLeaguesList = mRepository.getUserLeaguesList();
    }

    public LiveData<List<UserLeagues>> getUserLeaguesList() {
        return mUserLeaguesList;
    }

    public League getLeague(int id) {
        return mRepository.getLeague(id);
    }

}