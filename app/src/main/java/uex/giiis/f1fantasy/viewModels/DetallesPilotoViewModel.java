package uex.giiis.f1fantasy.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.pojo.DriverComplete;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.ui.DetallesPilotoActivity;

/**
 * {@link ViewModel} for {@link DetallesPilotoActivity}
 */
public class DetallesPilotoViewModel extends ViewModel {

    private final F1Repository mRepository;
    private LiveData<DriverComplete> mDriverComplete;
    private String mDriverId;

    public DetallesPilotoViewModel(F1Repository repository) {
        mRepository = repository;
        mDriverComplete = mRepository.getDriverComplete();
    }

    public void setDriverId (String driverId){
        mDriverId = driverId;
        mRepository.setDriverId(driverId);
    }

    public LiveData<DriverComplete> getDriverComplete() {
        return mDriverComplete;
    }

}