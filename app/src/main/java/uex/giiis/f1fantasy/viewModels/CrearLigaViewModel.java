package uex.giiis.f1fantasy.viewModels;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.generatedPojos.Result;
import uex.giiis.f1fantasy.pojo.ConstructorComplete;
import uex.giiis.f1fantasy.pojo.League;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.ui.popUps.CrearLigaPopup;
import uex.giiis.f1fantasy.repository.F1Repository;

/**
 * {@link ViewModel} for {@link CrearLigaPopup}
 */
public class CrearLigaViewModel extends ViewModel {

    private final F1Repository mRepository;


    public CrearLigaViewModel(F1Repository repository) {
        mRepository = repository;
    }

    public League getLeague(String username) {
        return mRepository.getLeague(username);
    }

    public void insertar(League league) {
        mRepository.insertLeague(league);
    }

    public void insertar(UserLeagues userLeagues) {
        mRepository.insertUserLeagues(userLeagues);
    }

}