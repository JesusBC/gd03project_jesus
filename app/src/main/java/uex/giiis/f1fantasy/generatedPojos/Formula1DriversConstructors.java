
package uex.giiis.f1fantasy.generatedPojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Formula1DriversConstructors {

    @SerializedName("MRData")
    @Expose
    private MRDataDriversConstructors mRDataDriversConstructors;

    @JsonProperty("MRData")
    public MRDataDriversConstructors getMRData() {
        return mRDataDriversConstructors;
    }

    public void setMRData(MRDataDriversConstructors mRDataDriversConstructors) {
        this.mRDataDriversConstructors = mRDataDriversConstructors;
    }

}
