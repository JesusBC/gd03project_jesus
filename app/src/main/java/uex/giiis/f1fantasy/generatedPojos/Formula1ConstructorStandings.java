
package uex.giiis.f1fantasy.generatedPojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Formula1ConstructorStandings {

    @SerializedName("MRData")
    @Expose
    private MRDataConstructorStandings mRDataConstructorStandings;

    @JsonProperty("MRData")
    public MRDataConstructorStandings getMRData() {
        return mRDataConstructorStandings;
    }

    public void setMRData(MRDataConstructorStandings mRDataConstructorStandings) {
        this.mRDataConstructorStandings = mRDataConstructorStandings;
    }

}
