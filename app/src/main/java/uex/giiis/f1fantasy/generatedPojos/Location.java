
package uex.giiis.f1fantasy.generatedPojos;

import androidx.room.Ignore;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Location {

    @Ignore
    @SerializedName("lat")
    @Expose
    private String lat;
    @Ignore
    @SerializedName("long")
    @Expose
    private String _long;
    @SerializedName("locality")
    @Expose
    private String locality;
    @SerializedName("country")
    @Expose
    private String country;

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("long")
    public String getLong() {
        return _long;
    }

    public void setLong(String _long) {
        this._long = _long;
    }

    @JsonProperty("locality")
    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
