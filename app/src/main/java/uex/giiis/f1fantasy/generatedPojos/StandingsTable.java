
package uex.giiis.f1fantasy.generatedPojos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StandingsTable {

    @SerializedName("season")
    @Expose
    private String season;
    @SerializedName("StandingsLists")
    @Expose
    private List<StandingsList> standingsLists = null;

    @JsonProperty("season")
    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    @JsonProperty("StandingsLists")
    public List<StandingsList> getStandingsLists() {
        return standingsLists;
    }

    public void setStandingsLists(List<StandingsList> standingsLists) {
        this.standingsLists = standingsLists;
    }

}
