
package uex.giiis.f1fantasy.generatedPojos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StandingsList {

    @SerializedName("season")
    @Expose
    private String season;
    @SerializedName("round")
    @Expose
    private String round;
    @SerializedName("DriverStandings")
    @Expose
    private List<DriverStanding> driverStandings = null;

    @JsonProperty("season")
    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    @JsonProperty("round")
    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    @JsonProperty("DriverStandings")
    public List<DriverStanding> getDriverStandings() {
        return driverStandings;
    }

    public void setDriverStandings(List<DriverStanding> driverStandings) {
        this.driverStandings = driverStandings;
    }

}
