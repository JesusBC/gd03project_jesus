
package uex.giiis.f1fantasy.generatedPojos;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;

@Entity(primaryKeys = {"round", "idDriver"},
        foreignKeys = {@ForeignKey(entity = Driver.class,
                parentColumns = "driverId",
                childColumns = "idDriver",
                onDelete = CASCADE),
                @ForeignKey(entity = Constructor.class,
                        parentColumns = "constructorId",
                        childColumns = "idConstructor",
                        onDelete = CASCADE)
        })
public class Result {

    @Ignore
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("position")
    @Expose
    private String position;
    @Ignore
    @SerializedName("positionText")
    @Expose
    private String positionText;
    @SerializedName("points")
    @Expose
    private String points;
    @Ignore
    @SerializedName("Driver")
    @Expose
    private Driver driver;
    @Ignore
    @SerializedName("Constructor")
    @Expose
    private Constructor constructor;
    @Ignore
    @SerializedName("grid")
    @Expose
    private String grid;
    @Ignore
    @SerializedName("laps")
    @Expose
    private String laps;
    @SerializedName("status")
    @Expose
    private String status;
    @Ignore
    @SerializedName("Time")
    @Expose
    private Time time;
    @Ignore
    @SerializedName("FastestLap")
    @Expose
    private FastestLap fastestLap;

    private String idConstructor;

    @NonNull
    private String idDriver;

    @NonNull
    private String round;

    public String getIdConstructor() {
        return idConstructor;
    }

    public void setIdConstructor(String idConstructor) {
        this.idConstructor = idConstructor;
    }

    public String getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(String idDriver) {
        this.idDriver = idDriver;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    @JsonProperty("number")
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @JsonProperty("position")
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @JsonProperty("positionText")
    public String getPositionText() {
        return positionText;
    }

    public void setPositionText(String positionText) {
        this.positionText = positionText;
    }

    @JsonProperty("points")
    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    @JsonProperty("Driver")
    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    @JsonProperty("Constructor")
    public Constructor getConstructor() {
        return constructor;
    }

    public void setConstructor(Constructor constructor) {
        this.constructor = constructor;
    }

    @JsonProperty("grid")
    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }

    @JsonProperty("laps")
    public String getLaps() {
        return laps;
    }

    public void setLaps(String laps) {
        this.laps = laps;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("Time")
    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    @JsonProperty("FastestLap")
    public FastestLap getFastestLap() {
        return fastestLap;
    }

    public void setFastestLap(FastestLap fastestLap) {
        this.fastestLap = fastestLap;
    }

}
