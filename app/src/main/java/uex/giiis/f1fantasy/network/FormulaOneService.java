package uex.giiis.f1fantasy.network;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import uex.giiis.f1fantasy.generatedPojos.Formula1Calendar;
import uex.giiis.f1fantasy.generatedPojos.Formula1ConstructorStandings;
import uex.giiis.f1fantasy.generatedPojos.Formula1DriversConstructors;
import uex.giiis.f1fantasy.generatedPojos.Formula1Results;
import uex.giiis.f1fantasy.generatedPojos.Formula1Standings;

public interface FormulaOneService {

    @GET("{season}/driverStandings.json")
    Call<Formula1Standings> getDriverStandings(@Path("season") String season);

    @GET("{season}.json")
    Call<Formula1Calendar> getCalendar(@Path("season") String season);

    @GET("{season}/constructorStandings.json")
    Call<Formula1ConstructorStandings> getConstructorStandings(@Path("season") String season);

    @GET("{season}/{round}/results.json")
    Call<Formula1Results> getResult(@Path("season") String season, @Path("round") int round);

    @GET("drivers/{idDriver}/constructors.json")
    Call<Formula1DriversConstructors> getDriverConstructors(@Path("idDriver") String idDriver);

    @GET("{season}/{round}/driverStandings.json")
    Call<Formula1Standings> getPreviousDriverStandings(@Path("season") String season, @Path("round") int round);

    @GET("{season}/{round}/constructorStandings.json")
    Call<Formula1ConstructorStandings> getPreviousConstructorStandings(@Path("season") String season, @Path("round") int round);

}
