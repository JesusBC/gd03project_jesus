package uex.giiis.f1fantasy.ui.popUps;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.util.List;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.pojo.League;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.CrearLigaViewModel;
import uex.giiis.f1fantasy.viewModels.LigasViewModel;

public class CrearLigaPopup extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_crear_liga_layout);

        DisplayMetrics medidasVentana = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(medidasVentana);

        int ancho = medidasVentana.widthPixels;
        int alto = medidasVentana.heightPixels;

        getWindow().setLayout((int) (ancho * 0.9), (int) (alto * 0.4));
    }

    public void cancelarCrear(View view) {
        onBackPressed();
    }

    public void crearLiga(View view) {

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        CrearLigaViewModel mViewModel = new ViewModelProvider(this, appContainer.crearLigaFactory)
                .get(CrearLigaViewModel.class);

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                EditText nombre = (EditText) findViewById(R.id.editar_nombre_crear);
                EditText pass = (EditText) findViewById(R.id.editar_password_crear);

                if (mViewModel.getLeague(nombre.getText().toString()) != null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            TextView error = (TextView) findViewById(R.id.error_crearLiga);
                            error.setText(R.string.crear_liga_error);
                        }
                    });
                } else {

                    if (!nombre.getText().toString().trim().isEmpty() && !pass.getText().toString().trim().isEmpty()) {
                        League league = new League();

                        SharedPreferences sharedPref = CrearLigaPopup.this.getSharedPreferences("usuario", CrearLigaPopup.this.MODE_PRIVATE);
                        int idUsuario = sharedPref.getInt("id_usuario", -1);

                        league.setOwner(idUsuario);
                        league.setName(nombre.getText().toString());
                        league.setPassword(pass.getText().toString());

                        mViewModel.insertar(league);

                        League updatedLeague = mViewModel.getLeague(nombre.getText().toString());
                        UserLeagues userLeague = new UserLeagues();
                        userLeague.setLeague(updatedLeague.getId());
                        userLeague.setUser(idUsuario);
                        userLeague.setPoints(0);
                        mViewModel.insertar(userLeague);

                        finish();
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView error = (TextView) findViewById(R.id.error_crearLiga);
                                error.setText(R.string.campo_vacio);
                            }
                        });
                    }

                }

            }
        });

    }
}
