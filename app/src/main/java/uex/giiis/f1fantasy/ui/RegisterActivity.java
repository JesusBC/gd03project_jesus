package uex.giiis.f1fantasy.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MainActivity;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.LoginViewModel;
import uex.giiis.f1fantasy.viewModels.RegisterViewModel;

public class RegisterActivity extends AppCompatActivity {

    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro_layout);

        sharedPref = RegisterActivity.this.getSharedPreferences("usuario", RegisterActivity.this.MODE_PRIVATE);

    }


    public void registrar(View v){

        EditText usuario = (EditText) findViewById(R.id.usuario_registrar);
        EditText password = (EditText) findViewById(R.id.password_registrar);
        EditText password_confirmar = (EditText) findViewById(R.id.password_confirmar_registrar);

        if(password.getText().toString().equals(password_confirmar.getText().toString())) {
            if (!usuario.getText().toString().trim().isEmpty() && !password.getText().toString().trim().isEmpty()) {

                AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
                RegisterViewModel mViewModel = new ViewModelProvider(this, appContainer.registerFactory)
                        .get(RegisterViewModel.class);
                mViewModel.setUsername(usuario.getText().toString());
                mViewModel.getUser().observe(this, new Observer<User>() {

                    @Override
                    public void onChanged(User user) {
                        //Comprobar que no exista el usuario en la base de datos
                        if(user == null){

                            //Insertar usuario en la base de datos
                            User usuarioInsertar = new User();
                            usuarioInsertar.setUsername(usuario.getText().toString());
                            usuarioInsertar.setPassword(password.getText().toString());

                            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                                @Override
                                public void run() {
                                    long idInsercion = mViewModel.insertUser(usuarioInsertar);

                                    //Actualizar preferencias del usuario
                                    SharedPreferences.Editor editor = sharedPref.edit();
                                    editor.putInt("id_usuario", (int) idInsercion);
                                    editor.commit();

                                    iniciarMainActivity();
                                }
                            });

                        }else{
                            TextView errorView = (TextView) findViewById(R.id.errorRegistro);
                            errorView.setText(R.string.register_failed_user);
                        }

                    }
                });

            } else {
                //Campos vacíos
                TextView msgError = (TextView) findViewById(R.id.errorRegistro);
                msgError.setText(R.string.campo_vacio);
            }
        }else{
            //Contraseñas diferentes
            TextView msgError = (TextView) findViewById(R.id.errorRegistro);
            msgError.setText(R.string.register_failed);
        }


    }

    public void irLogin(View v){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void iniciarMainActivity(){
        Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}