package uex.giiis.f1fantasy.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.LoginViewModel;
import uex.giiis.f1fantasy.viewModels.PerfilViewModel;

public class PerfilActivity extends AppCompatActivity {

    SharedPreferences sharedPref;
    int id_usuario;
    private PerfilViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        sharedPref = PerfilActivity.this.getSharedPreferences("usuario", PerfilActivity.this.MODE_PRIVATE);
        id_usuario = sharedPref.getInt("id_usuario", -1);

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.perfilFactory)
                .get(PerfilViewModel.class);
        mViewModel.setId(id_usuario);
        mViewModel.getUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {

                sharedPref = PerfilActivity.this.getSharedPreferences("usuario", PerfilActivity.this.MODE_PRIVATE);
                id_usuario = sharedPref.getInt("id_usuario", -1);

                if(id_usuario != -1) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            TextView saludo = (TextView) findViewById(R.id.titulo_perfil);
                            saludo.append(user.getUsername() + "!");

                            EditText nombre = (EditText) findViewById(R.id.usuario_perfil);
                            nombre.setText(user.getUsername());

                            EditText pass = (EditText) findViewById(R.id.password_perfil);
                            pass.setText(user.getPassword());
                        }
                    });
                }

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_app_bar_perfil, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void borrarPerfil (View view) {
        sharedPref = PerfilActivity.this.getSharedPreferences("usuario", PerfilActivity.this.MODE_PRIVATE);
        id_usuario = sharedPref.getInt("id_usuario", -1);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("id_usuario", -1);
        editor.commit();

        AppExecutors.getInstance().diskIO().execute(new Runnable() {

            @Override
            public void run() {

                int deleted = mViewModel.deleteUser(id_usuario);

                if(deleted > 0){

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            volverLogin();
                        }
                    });
                }
            }
        });
    }

    public void aceptarCambios (View view) {
        sharedPref = PerfilActivity.this.getSharedPreferences("usuario", PerfilActivity.this.MODE_PRIVATE);
        id_usuario = sharedPref.getInt("id_usuario", -1);

        AppExecutors.getInstance().diskIO().execute(new Runnable() {

            @Override
            public void run() {
                EditText usuario = (EditText) findViewById(R.id.usuario_perfil);
                EditText password = (EditText) findViewById(R.id.password_perfil);

                int updated = mViewModel.updateUser(id_usuario, usuario.getText().toString(), password.getText().toString());

                if(updated > 0){

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            TextView titulo = (TextView) findViewById(R.id.titulo_perfil);
                            titulo.setText("¡Perfil actualizado, ");
                        }
                    });
                }
            }
        });
    }

    public void volverLogin () {
        Intent intent = new Intent (this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}