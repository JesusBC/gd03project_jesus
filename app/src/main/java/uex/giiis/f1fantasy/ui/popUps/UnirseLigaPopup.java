package uex.giiis.f1fantasy.ui.popUps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.pojo.League;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.LigasViewModel;
import uex.giiis.f1fantasy.viewModels.UnirseLigaViewModel;

public class UnirseLigaPopup extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_unirse_liga_layout);

        DisplayMetrics medidasVentana = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(medidasVentana);

        int ancho = medidasVentana.widthPixels;
        int alto = medidasVentana.heightPixels;

        getWindow().setLayout((int) (ancho * 0.9), (int) (alto * 0.4));
    }

    public void cancelarUnirse (View view) {
        onBackPressed();
    }

    public void unirseLiga (View view) {

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        UnirseLigaViewModel mViewModel = new ViewModelProvider(this, appContainer.unirseLigaFactory)
                .get(UnirseLigaViewModel.class);

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                EditText idLiga = (EditText) findViewById(R.id.editar_nombre_unirse);
                EditText pass = (EditText) findViewById(R.id.editar_password_unirse);

                League league = mViewModel.getLeague(idLiga.getText().toString(), pass.getText().toString());

                if (league == null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            TextView error = (TextView) findViewById(R.id.error_unirseLiga);
                            error.setText(R.string.unirse_liga_error);
                        }
                    });
                } else {
                    SharedPreferences sharedPref = UnirseLigaPopup.this.getSharedPreferences("usuario", UnirseLigaPopup.this.MODE_PRIVATE);
                    int idUsuario = sharedPref.getInt("id_usuario", -1);
                    UserLeagues userLeague = new UserLeagues();
                    userLeague.setLeague(league.getId());
                    userLeague.setUser(idUsuario);
                    userLeague.setPoints(0);
                    mViewModel.insertar(userLeague);
                    finish();
                }
            }
        });

    }

}