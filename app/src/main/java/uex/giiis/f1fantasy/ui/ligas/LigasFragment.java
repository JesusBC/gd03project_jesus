package uex.giiis.f1fantasy.ui.ligas;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.pojo.ConstructorComplete;
import uex.giiis.f1fantasy.recyclerViews.LigasRecyclerViewAdapter;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.pojo.League;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.EscuderiasViewModel;
import uex.giiis.f1fantasy.viewModels.LigasViewModel;

public class LigasFragment extends Fragment{

    private LigasViewModel ligasViewModel;
    private LigasRecyclerViewAdapter adapter;
    private List<League> leagues;
    private F1Repository mRepository;
    private LigasRecyclerViewAdapter.OnListInteractionListener mCallback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        leagues = new ArrayList<>();
        ligasDatabase();
        adapter = new LigasRecyclerViewAdapter(leagues, mCallback, getContext());

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ligas, container, false);

        RecyclerView recyclerView;
        RecyclerView.LayoutManager layoutManager;

        recyclerView = (RecyclerView) view.findViewById(R.id.list_ligas);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LigasRecyclerViewAdapter.OnListInteractionListener) {
            mCallback = (LigasRecyclerViewAdapter.OnListInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListInteractionListener");
        }
    }


    private void actualizarRecyclerView(List<League> updateLeagues) {
        adapter.swap(updateLeagues);
    }

    public void ligasDatabase () {

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        LigasViewModel mViewModel = new ViewModelProvider(this, appContainer.ligasFactory)
                .get(LigasViewModel.class);
        mViewModel.getUserLeaguesList().observe(this, new Observer<List<UserLeagues>>() {

            @Override
            public void onChanged(List<UserLeagues> userLeaguesList) {

                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        leagues.clear();
                        for (UserLeagues ul : userLeaguesList) {
                            League league = mViewModel.getLeague(ul.getLeague());
                            leagues.add(league);
                        }

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                actualizarRecyclerView(leagues);
                                if (adapter.getItemCount() != 0) {
                                    TextView text = (TextView) getView().findViewById(R.id.texto_ligas);
                                    text.setText(" ");
                                    ImageView image = (ImageView) getView().findViewById(R.id.foto_ligas);
                                    image.setImageResource(0);
                                }
                            }
                        });
                    }
                });


            }
        });


    }

}