package uex.giiis.f1fantasy.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.pojo.UserLeaguesComplete;
import uex.giiis.f1fantasy.recyclerViews.SeleccionarPilotosRecyclerViewAdapter;
import uex.giiis.f1fantasy.recyclerViews.UsuariosExpandableListAdapter;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.LoginViewModel;
import uex.giiis.f1fantasy.viewModels.VerPilotosElegidosViewModel;

public class VerPilotosElegidosActivity extends AppCompatActivity {

    private SharedPreferences sharedPref;
    private UsuariosExpandableListAdapter adapter;
    private List<String> usuarios;
    private ExpandableListView expandableListView;
    private Map<String, List<String>> pilotosUsuario;

    private int idLiga;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_pilotos_elegidos);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        Bundle bundle = getIntent().getExtras();
        idLiga = Integer.parseInt(bundle.getString("ID_LIGA"));

        expandableListView = (ExpandableListView) findViewById(R.id.list_pilotos_elegidos_usuario);

        usuarios = new ArrayList<String>();
        pilotosUsuario = new HashMap<String, List<String>>();

        adapter = new UsuariosExpandableListAdapter(usuarios, pilotosUsuario,this);

        expandableListView.setAdapter(adapter);

        datos();

    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_app_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void datos(){

        pilotosUsuario.clear();
        usuarios.clear();

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        VerPilotosElegidosViewModel mViewModel = new ViewModelProvider(this, appContainer.verPilotosElegidosFactory)
                .get(VerPilotosElegidosViewModel.class);
        mViewModel.setIdLiga(idLiga);
        mViewModel.getFromLeague().observe(this, new Observer<List<UserLeaguesComplete>>() {
            @Override
            public void onChanged(List<UserLeaguesComplete> userLeaguesCompletes) {

                pilotosUsuario.clear();
                usuarios.clear();
                for (UserLeaguesComplete userLeaguesComplete : userLeaguesCompletes) {

                    List<String> listaPilotos = new ArrayList<String>();
                    if (userLeaguesComplete.getDriver1() != null && userLeaguesComplete.getDriver2() != null && userLeaguesComplete.getDriver3() != null) {
                        String username = userLeaguesComplete.getUser().getUsername();
                        listaPilotos.add(userLeaguesComplete.getDriver1().getGivenName() + " " + userLeaguesComplete.getDriver1().getFamilyName());
                        listaPilotos.add(userLeaguesComplete.getDriver2().getGivenName() + " " + userLeaguesComplete.getDriver2().getFamilyName());
                        listaPilotos.add(userLeaguesComplete.getDriver3().getGivenName() + " " + userLeaguesComplete.getDriver3().getFamilyName());
                        pilotosUsuario.put(username, listaPilotos);
                        usuarios.add(username);
                    }
                }

                actualizarExpandableView();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    private void actualizarExpandableView() {
        adapter.swap(usuarios, pilotosUsuario);
        for(int i=0; i < adapter.getGroupCount(); i++)
            expandableListView.expandGroup(i);
    }

    public void editarPerfil (MenuItem item) {
        Intent intent = new Intent(this, PerfilActivity.class);
        startActivity(intent);
    }

    public void cerrarSesion (MenuItem item) {
        sharedPref = VerPilotosElegidosActivity.this.getSharedPreferences("usuario", VerPilotosElegidosActivity.this.MODE_PRIVATE);

        int id_usuario = sharedPref.getInt("id_usuario", -1);

        if (id_usuario != -1) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("id_usuario", -1);
            editor.commit();

            Intent intent = new Intent (this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }


}