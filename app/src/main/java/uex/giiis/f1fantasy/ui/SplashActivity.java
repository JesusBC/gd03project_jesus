package uex.giiis.f1fantasy.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.widget.ProgressBar;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MainActivity;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.network.F1NetworkDataSource;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.network.LoaderRunnable;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ProgressBar progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress.getIndeterminateDrawable()
                .setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        //Container para el daoCollection
        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;

        //Necesitamos el F1NetWorkDataSource
        F1NetworkDataSource mF1NetworkDataSource = F1NetworkDataSource.getInstance(this);

        F1Repository repository = F1Repository.getInstance(appContainer.daoCollection, mF1NetworkDataSource);

        //LLAMAR AL METODO FETCH DEL REPOSITORY PARA QUE EMPIECE A OBTENER DATOS DE LA API
        repository.doFetchData(this);
    }

    public void finishSplash () {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}