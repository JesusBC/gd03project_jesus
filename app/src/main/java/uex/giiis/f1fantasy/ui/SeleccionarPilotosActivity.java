package uex.giiis.f1fantasy.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.pojo.DriverComplete;
import uex.giiis.f1fantasy.recyclerViews.SeleccionarPilotosRecyclerViewAdapter;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.DetallesLigaViewModel;
import uex.giiis.f1fantasy.viewModels.SeleccionarPilotosViewModel;

public class SeleccionarPilotosActivity extends AppCompatActivity implements SeleccionarPilotosRecyclerViewAdapter.OnSeleccionarPilotosInteractionListener {

    private SharedPreferences sharedPref;
    private SeleccionarPilotosRecyclerViewAdapter adapter;
    private List<Driver> drivers;
    private UserLeagues userLeagues;
    private List<String> pilotosUsuario;
    private SeleccionarPilotosRecyclerViewAdapter.OnSeleccionarPilotosInteractionListener mCallback;
    private int idLiga;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seleccion_pilotos_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        Bundle bundle = getIntent().getExtras();
        idLiga = Integer.parseInt(bundle.getString("ID_LIGA"));
        drivers = new ArrayList<Driver>();
        mCallback = (SeleccionarPilotosRecyclerViewAdapter.OnSeleccionarPilotosInteractionListener) this;
        adapter = new SeleccionarPilotosRecyclerViewAdapter(drivers, null ,mCallback, this);

        pilotosUsuario = new ArrayList<String>();

        RecyclerView recyclerView;
        RecyclerView.LayoutManager layoutManager;

        recyclerView = (RecyclerView) findViewById(R.id.list_pilotos);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        recyclerView.setAdapter(adapter);

        driversDatabase();
        pilotosUsuarioDatabase();

    }


    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_app_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void aceptarPilotos(View v){
        if(pilotosUsuario.size() != 3) {
            Toast toast = Toast.makeText(this, "Debes seleccionar 3 pilotos",Toast.LENGTH_SHORT);
            toast.show();
        }else{

            userLeagues.setDriver1(pilotosUsuario.get(0));
            userLeagues.setDriver2(pilotosUsuario.get(1));
            userLeagues.setDriver3(pilotosUsuario.get(2));

            AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
            SeleccionarPilotosViewModel mViewModel = new ViewModelProvider(this, appContainer.seleccionarPilotosFactory)
                    .get(SeleccionarPilotosViewModel.class);

            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    mViewModel.update(userLeagues);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            onBackPressed();
                        }
                    });
                }
            });
        }
    }

    public void cancelarPilotos(View v){
        onBackPressed();
    }


    @Override
    public void onResume(){
        super.onResume();
    }

    private void pilotosUsuarioRecyclerView(List<String> _pilotosUsuario){
        adapter.pilotosSeleccionadorsUsuario(_pilotosUsuario);
    }
    private void actualizarRecyclerView(List<Driver> updateDrivers) {
        adapter.swap(updateDrivers);
    }

    private void pilotosUsuarioDatabase(){
        SharedPreferences sharedPref = this.getSharedPreferences("usuario", this.MODE_PRIVATE);

        int id_usuario = sharedPref.getInt("id_usuario", -1);

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        SeleccionarPilotosViewModel mViewModel = new ViewModelProvider(this, appContainer.seleccionarPilotosFactory)
                .get(SeleccionarPilotosViewModel.class);
        mViewModel.setIdLigaIdPiloto(id_usuario, idLiga);
        mViewModel.getUserLeagues().observe(this, new Observer<UserLeagues>() {
            @Override
            public void onChanged(UserLeagues userLeaguesDB) {
                List<String> _pilotosUsuaio = new ArrayList<String>();

                userLeagues = userLeaguesDB;

                if(userLeagues.getDriver1() != null){
                    _pilotosUsuaio.add(userLeagues.getDriver1());
                    _pilotosUsuaio.add(userLeagues.getDriver2());
                    _pilotosUsuaio.add(userLeagues.getDriver3());
                }


                pilotosUsuarioRecyclerView(_pilotosUsuaio);
                pilotosUsuario = _pilotosUsuaio;
            }
        });
    }

    public void driversDatabase () {

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        SeleccionarPilotosViewModel mViewModel = new ViewModelProvider(this, appContainer.seleccionarPilotosFactory)
                .get(SeleccionarPilotosViewModel.class);
        mViewModel.getAllDrivers().observe(this, new Observer<List<Driver>>() {
            @Override
            public void onChanged(List<Driver> allDrivers) {
                drivers = allDrivers;
                actualizarRecyclerView(drivers);
            }
        });
    }

    @Override
    public void OnSeleccionarPilotosInteractionListener(String driverId ,boolean _checked) {
        //Si marca
        if(_checked){
            Log.d(null,"añadimos el piloto " + driverId);
            pilotosUsuario.add(driverId);
        }else{
            pilotosUsuario.remove(driverId);
        }
        pilotosUsuarioRecyclerView(pilotosUsuario);
        String texto = "Pilotos restantes: " + (3-pilotosUsuario.size());
        if(pilotosUsuario.size() == 3){
            texto = "Ya has seleccionado todos los pilotos!";
        }
        Toast toast = Toast.makeText(this, texto,Toast.LENGTH_SHORT);
        toast.show();
    }


    public void editarPerfil (MenuItem item) {
        Intent intent = new Intent(this, PerfilActivity.class);
        startActivity(intent);
    }

    public void cerrarSesion (MenuItem item) {
        sharedPref = SeleccionarPilotosActivity.this.getSharedPreferences("usuario", SeleccionarPilotosActivity.this.MODE_PRIVATE);

        int id_usuario = sharedPref.getInt("id_usuario", -1);

        if (id_usuario != -1) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("id_usuario", -1);
            editor.commit();

            Intent intent = new Intent (this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }


}