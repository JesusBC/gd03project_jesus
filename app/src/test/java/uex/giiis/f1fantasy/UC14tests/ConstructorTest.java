package uex.giiis.f1fantasy.UC14tests;

import uex.giiis.f1fantasy.generatedPojos.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @see <a href="http://d.android.com/tools/testing%22%3ETesting documentation</a>
 */
public class ConstructorTest {

    private Constructor constructor;

    @Test
    public void constructorShouldBeCreated() {
        constructor = new Constructor();
        constructor.setConstructorId("mercedes");
        constructor.setName("Mercedes AMG Petronas");
        constructor.setNationality("UK");
        constructor.setUrl("www.mercedes.com");

        assertEquals("mercedes", constructor.getConstructorId());
        assertEquals("Mercedes AMG Petronas", constructor.getName());
        assertEquals("UK", constructor.getNationality());
        assertEquals("www.mercedes.com", constructor.getUrl());
    }
}
