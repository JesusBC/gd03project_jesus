package uex.giiis.f1fantasy.UC15tests;

import uex.giiis.f1fantasy.generatedPojos.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @see <a href="http://d.android.com/tools/testing%22%3ETesting documentation</a>
 */
public class GrandPrixTest {

    private Race gp;
    private Circuit circuit;
    private Location location;

    @Test
    public void grandPrixShouldBeCreated() {
        gp = new Race();
        circuit = new Circuit();
        location = new Location();

        location.setLocality("Barcelona");
        location.setCountry("Spain");

        circuit.setCircuitId("catalunya");
        circuit.setCircuitName("Circuit de Barcelona");
        circuit.setLocation(location);

        gp.setCircuit(circuit);
        gp.setDate("18/05/2020");
        gp.setRaceName("Gran Premio de España");
        gp.setRound("5");
        gp.setSeason("2020");
        gp.setTime("15:10");

        assertEquals("18/05/2020", gp.getDate());
        assertEquals("Gran Premio de España", gp.getRaceName());
        assertEquals("5", gp.getRound());
        assertEquals("2020", gp.getSeason());
        assertEquals("15:10", gp.getTime());

        assertEquals("catalunya", gp.getCircuit().getCircuitId());
        assertEquals("Circuit de Barcelona", gp.getCircuit().getCircuitName());

        assertEquals("Barcelona", gp.getCircuit().getLocation().getLocality());
        assertEquals("Spain", gp.getCircuit().getLocation().getCountry());
    }
}
