package uex.giiis.f1fantasy.UC02tests;

import uex.giiis.f1fantasy.pojo.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing%22%3ETesting documentation</a>
 */
public class UserLeaguesTest {

    private UserLeagues userLeagues;

    @Test
    public void leagueShouldBeCreated() {
        userLeagues = new UserLeagues();
        userLeagues.setLeague(11);
        userLeagues.setUser(23);
        userLeagues.setDriver1("hamilton");
        userLeagues.setDriver2("sainz");
        userLeagues.setDriver3("russell");
        userLeagues.setPoints(392);

        assertEquals(11, userLeagues.getLeague());
        assertEquals(23, userLeagues.getUser());
        assertEquals("hamilton", userLeagues.getDriver1());
        assertEquals("sainz", userLeagues.getDriver2());
        assertEquals("russell", userLeagues.getDriver3());
        assertEquals(392, userLeagues.getPoints());

    }
}
