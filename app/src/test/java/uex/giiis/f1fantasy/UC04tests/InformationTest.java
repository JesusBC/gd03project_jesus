package uex.giiis.f1fantasy.UC04tests;

import uex.giiis.f1fantasy.pojo.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @see <a href="http://d.android.com/tools/testing%22%3ETesting documentation</a>
 */
public class InformationTest {

    private Information information;

    @Test
    public void informationShouldBeCreated() {
        information = new Information();

        assertEquals(null, information.getSeason());
        assertEquals(null, information.getRound());

        information.setSeason("2020");
        information.setRound("13");

        assertEquals("2020", information.getSeason());
        assertEquals("13", information.getRound());

    }
}