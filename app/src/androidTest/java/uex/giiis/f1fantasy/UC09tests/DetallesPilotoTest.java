package uex.giiis.f1fantasy.UC09tests;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.ui.LoginActivity;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class DetallesPilotoTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void detallesPilotoTest() {

        onView(withId(R.id.boton_registro_login)).perform(click());

        onView(withId(R.id.usuario_registrar)).perform(typeText("UserPrueba"));
        onView(withId(R.id.password_registrar)).perform(typeText("1234"));
        onView(withId(R.id.password_confirmar_registrar)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.boton_registro_online)).perform(click());

        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.navigation_escuderias)).perform(click());

        onView(withId(R.id.navigation_escuderias)).perform(click());
        onData(anything()).inAdapterView(withId(R.id.list_escuderias)).atPosition(0).perform(click());
        onView(withText("Lewis Hamilton")).perform(click());

        onView(withId(R.id.nombre_piloto))
                .perform(click())
                .check(matches(not(withText("nombre de piloto"))));

        onView(withId(R.id.texto_nacionalidad))
                .perform(click())
                .check(matches(isDisplayed()));

        onView(withId(R.id.texto_constructor_actual))
                .perform(click())
                .check(matches(isDisplayed()));

        openContextualActionModeOverflowMenu();
        onView(withText(R.string.perfil)).perform(click());
        onView(withId(R.id.boton_borrar)).perform(click());
    }

}
