package uex.giiis.f1fantasy.UC16tests;

import androidx.test.espresso.NoMatchingViewException;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.ui.LoginActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class MostrarClasificacionLigaTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void shouldShowLeagueStanding() {

        onView(withId(R.id.boton_registro_login)).perform(click());

        onView(withId(R.id.usuario_registrar)).perform(typeText("UserPrueba"));
        onView(withId(R.id.password_registrar)).perform(typeText("1234"));
        onView(withId(R.id.password_confirmar_registrar)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.boton_registro_online)).perform(click());

        boolean salir = false;
        while (!salir) {
            try {
                onView(withId(R.id.navigation_calendario)).check(matches(isDisplayed()));
                salir = true;
            } catch (NoMatchingViewException e) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        }

        onView(withId(R.id.navigation_ligas)).perform(click());

        // Creamos una liga
        onView(withId(R.id.boton_crear_liga)).perform(click());
        onView(withId(R.id.editar_nombre_crear)).perform(typeText("Liga de Prueba"));
        onView(withId(R.id.editar_password_crear)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.boton_crear_liga_liga)).perform(click());

        // Comprobamos que la liga se encuentre en nuestra lista de ligas
        onView(withId(R.id.list_ligas)).check(matches(hasDescendant(withId(R.id.nombre_liga))));
        onView(withId(R.id.list_ligas)).check(matches(hasDescendant(withText("Liga de Prueba"))));

        // Vemos la clasificacion de la liga
        onView(withText("Liga de Prueba")).perform(click());

        // Comprobamos que el usuario se encuentra en la liga
        onView(withId(R.id.list_usuarios_clasificacion_liga)).check(matches(hasDescendant(withId(R.id.usuario_liga))));
        onView(withId(R.id.list_usuarios_clasificacion_liga)).check(matches(hasDescendant(withText("UserPrueba"))));
        // Comprobamos que el usuario va el primero
        onView(withId(R.id.list_usuarios_clasificacion_liga)).check(matches(hasDescendant(withId(R.id.puesto_liga))));
        onView(withId(R.id.list_usuarios_clasificacion_liga)).check(matches(hasDescendant(withText("1"))));
        // Comprobamos que el usuario no tiene puntos
        onView(withId(R.id.list_usuarios_clasificacion_liga)).check(matches(hasDescendant(withId(R.id.puntuacion_liga))));
        onView(withId(R.id.list_usuarios_clasificacion_liga)).check(matches(hasDescendant(withText("0"))));

        openContextualActionModeOverflowMenu();
        onView(withText(R.string.perfil)).perform(click());
        onView(withId(R.id.boton_borrar)).perform(click());
    }
}

